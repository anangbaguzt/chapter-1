const axios = require('axios');
const tes = "tes";

getDataAsyncAwait();
// getDataCallback();

async function getDataAsyncAwait(){
    try{
        const response = await axios('https://indonesia-public-static-api.vercel.app/api/heroes');
        const data = response.data;
        console.log(data);
    }catch(err){
        console.error(err.message);
    }
}

function getDataCallback(){
    axios.get('https://indonesia-public-static-api.vercel.app/api/heroes')
    .then(response => {
        console.log(data);
    })
    .catch(err => {
        console.error(err.message);
    });
}